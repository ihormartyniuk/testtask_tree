<?php

interface MethodInterface
{
    /**
     * @param array $tree
     * @return mixed
     */
    public function setOriginTree(array $tree);

    /**
     * @param $weight
     * @return mixed
     */
    public function setNodeLeafsMaxWeight(int $weight);

    /**
     * @return array
     */
    public function getSortedTree():array;
}