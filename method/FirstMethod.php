<?php
include('MethodInterface.php');

class FirstMethod implements MethodInterface
{
    private $origin_tree=[];
    private $node_leaves_max_weight;

    private $temp_lefted_leaves = [];

    /**
     * @param array $tree
     */
    public function setOriginTree(array $tree)
    {
        $this->origin_tree = $tree;
    }

    /**
     * @param $weight
     */
    public function setNodeLeafsMaxWeight(int $weight)
    {
        $this->node_leaves_max_weight = $weight;
    }

    /**
     * @return array
     */
    public function getSortedTree(): array
    {
        return $this->handleNode($this->origin_tree);
    }

    /**
     * @param array $node
     * make operation under one node item
     * @return array
     */

    private function handleNode($node)
    {
        $leaves = [];
        $child_nodes = [];

        /**
         *  by that structure of data and task condition we need first get sorted leaves
         *  for right leaves calculation in child nodes
         *  and the next step will be handle child nodes
         */

        foreach ($node as $key => $item)
        {
            if (is_array($item))
            {
                $child_nodes[] = $item;
                unset($node[$key]);
            }
            else{
                $leaves[] = $item;
                unset($node[$key]);
            }
        }

        $leaves = $this->handleNodeLeaves($leaves);
        $node = array_merge($node, $leaves);

        foreach ($child_nodes as $child_node) $node[] = $this->handleNode($child_node);

        return $node;
    }

    /**
     * @param array $leaves
     * make operation under leaves of one node item
     * @return array
     */
    private function handleNodeLeaves(array $leaves):array
    {
        $result = [];
        $weight = 0;
        $leaves = array_merge($leaves, $this->temp_lefted_leaves);
        $sorted_leaves = $this->sortNodeLeafs($leaves);

        foreach ($sorted_leaves as $key => $leaf_weight)
        {
            if (($weight+$leaf_weight) <= $this->node_leaves_max_weight)
            {
                $result[]=$leaf_weight;
                $weight += $leaf_weight;

                unset($sorted_leaves[$key]);
            } else break;
        }

        $this->temp_lefted_leaves = $sorted_leaves;
        return $result;

    }

    /**
     * @param array $leaves
     * make Bubble Sort algorithm for sorting leaves
     * @return array
     */
    private function sortNodeLeafs(array $leaves):array
    {
        for ($i=0; $i<=count($leaves); $i++)
        {
            for ($k=0; $k<count($leaves)-1; $k++)
            {
                $value1 = $leaves[$k];
                $value2 = $leaves[$k+1];

                if ($value1 > $value2)
                {
                    $leaves[$k]=$value2;
                    $leaves[$k+1] = $value1;
                }
            }
        }

        return $leaves;
    }

}