<?php

class TreeManager
{
    private $tree;

    public function __construct(MethodInterface $method, $origin_tree, $nodeLeafsMaxWeight)
    {
        $method->setOriginTree($origin_tree);
        $method->setNodeLeafsMaxWeight($nodeLeafsMaxWeight);
        $this->tree = $method->getSortedTree();
    }

    public function getTree()
    {
        return $this->tree;
    }
}