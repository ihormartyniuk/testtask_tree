<?php
include('TreeManager.php');
include('method/FirstMethod.php');

$tree = [
    [
        1, 1, 1, 1, 1,
        [
            1,2,6,3,
            [
                3,6,
                [
                    4,8,2,
                ]
            ]
        ],
        [
            9,1,2
        ]
    ],
];

$treeNodeLeafsMaxWeight = 4;
$method = new FirstMethod();
$treeManager = new TreeManager($method, $tree, $treeNodeLeafsMaxWeight);

print_r($treeManager->getTree());