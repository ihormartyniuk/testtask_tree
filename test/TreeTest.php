<?php
require_once('/var/www/testtask_interkassatree/TreeManager.php');
require_once('/var/www/testtask_interkassatree/method/FirstMethod.php');

use PHPUnit\Framework\TestCase;

/**
 * Using PHPUnit
 * installing via composer
 *
 */
class TreeTest extends TestCase {


    public function testEntireParams1(){

        $this->expectException('TypeError');

        $method = new FirstMethod();
        new TreeManager($method, null, 2);
    }

    public function testEntireParams2()
    {
        $method = new FirstMethod();
        $treeManager = new TreeManager($method, [], 2);
        $this->assertEquals([], $treeManager->getTree());
    }


    public function testFirstMethodClass1()
    {
        $method = new FirstMethod();
        $this->assertEquals(null, $method->setOriginTree([]));
    }

    public function testFirstMethodClass2()
    {
        $this->expectException('TypeError');

        $method = new FirstMethod();
        $method->setNodeLeafsMaxWeight('three');
    }

}


